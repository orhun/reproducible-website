---
layout: event_detail
title: Collaborative Working Sessions - Born Reproducible I
event: hamburg2023
order: 213
permalink: /events/hamburg2023/born-reproducible-1/
---

- Who is running it
- Who is running builds
- At what granularity are build steps defined
- What os/platform are necessary to support
- How (or if) non-owned build inputs are fetched/supported
- What are the different "stacks" that run builders
- How closely does "build input" reflect the full set of things that can inpact output
- How "explicit" is the build definition

- Rebuild evidence
- (re)builder indentity
- Both successes and failures to rebuild


What are we trying to do?
* Understand build diffs
* Build integrity <- many similar builders
* Build malice <- many different builders
* Rebuild debugging/detection
  * Transient mismatch
  * Deterministic mismatch
* Rebuild smells <- environment variation injector (e.g., build diversity fuzzer)

What are the techniques that can help?
* File system isolation
* Ephemeral environment
* Deterministic Scheduling
* Multiple sequential rebuilds
