---
layout: event_detail
title: Collaborative Working Sessions - Guix To-Do's
event: hamburg2023
order: 203
permalink: /events/hamburg2023/guix-todo/
---

# Long term goals:
    
- All packages build reproducibly
  - Benefits security
  - Future proofing
  
- K of N trust in substitutes (where K > 1) 
  - Benefits security
      
# Things related to reproducible builds

 - The data service info
 - `guix challenge`
 - `guix build --check` and `guix build --rounds`
 
TODO list:
    - build with disorderfs
    - linter for matching substitutes (to flag non reproducible packages)
    - QA checking reproducibility in patches/branches
    - User submitted build results
    - Prioritised list of packages/issues to fix
    
# Actionable items

## Some kind of guix buildinfo

That you can submit to the data service to describe a build you've done. Would be useful from the build coordinator but also submitted from users. This would help to find non-reproducible packages.

## QA doing builds to test reproducibility

## Improve qa.guix.gnu.org/reproducible-builds

Check and prioritise issues.

## Track package reproducibility percentage over time

And backfill data.

## Implement K of N trust in substitutes 

See https://lists.gnu.org/archive/html/guix-devel/2020-06/msg00179.html
