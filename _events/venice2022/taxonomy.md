---
layout: event_detail
title: Collaborative Working Sessions - Taxonomy
event: venice2022
order: 110
permalink: /events/venice2022/taxonomy
---

Reproducible Builds Summit 2022


Aim: Map distros/package managers on to a linear scale about how difficult it is to build things reproducibly

Not attempting to solve any problems, but just describing the space. We also didn't know everything we needed to know to place things accurately, so there might be some errors.



Tool:
 * Warpforge / Golang (modules)
  * Below this point, bytes/hashes of build inputs are not necessarily the same
 * GNU Guix / Nix
  * Below this point, the specific packages/versions used in a build aren't entirely specifyed by the source package alone
 * Debian / Ubuntu / Fedora
  * Below this point, all the inputs aren't necessarily captured by the tool, but there might be some description of "foreign" inputs
 * Cabal / Stack / Opam / Cargo
 * Maven
  * Below this point there isn't a description of "foreign" inputs
 * NPM
 * Rubygems / Pip



We made a few categories about how the above tools differ:
    
 * How source and inputs are referenced
  * Specific bytes/hash
  * Complete description of build process
  * Name/version
  * ...
      
 * How versions are handled
  * Versions are fully specified in the source definition
  * Versions are decided at build time
  * ...
     
 * How "foreign" dependencies are handled?
  * No "foreign" dependencies, a complete description of what is used
  * Some support for specifying "foreign" dependencies
  * No assistance around foreign dependencies

