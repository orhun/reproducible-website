---
layout: report
year: "2023"
month: "02"
title: "Reproducible Builds in February 2023"
draft: false
date: 2023-03-05 08:53:40
---

[![]({{ "/images/reports/2023-02/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

**Welcome to the February 2023 report from the [Reproducible Builds](https://reproducible-builds.org) project.** As ever, if you are interested in contributing to our project, please visit the [*Contribute*]({{ "/contribute/" | relative_url }}) page on our website.

---

[![]({{ "/images/reports/2023-02/fosdem.jpeg#right" | relative_url }})](FOSDEM)

[FOSDEM 2023](https://fosdem.org/2023/) was held in Brussels on the 4th & 5th of February and featured a number of talks related to reproducibility. In particular, Akihiro Suda gave a talk titled [*Bit-for-bit reproducible builds with Dockerfile*](https://fosdem.org/2023/schedule/event/container_reproducible_dockerfile) discussing deterministic timestamps and deterministic `apt-get` ([original announcement](https://lists.reproducible-builds.org/pipermail/rb-general/2023-February/002842.html)). There was also an entire ['track' of talks on Software Bill of Materials](https://fosdem.org/2023/schedule/track/software_bill_of_materials/) (SBOMs). SBOMs are an inventory for software with the intention of increasing the transparency of software components (the US [National Telecommunications and Information Administration](https://ntia.gov/) (NTIA) published a useful [*Myths vs. Facts*](https://ntia.gov/sites/default/files/publications/sbom_myths_vs_facts_nov2021_0.pdf) document in 2021).

<br>

On [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/) this month, Larry Doolittle was puzzled why the [Debian `verilator` package](https://tracker.debian.org/pkg/verilator) was not reproducible [[...](https://lists.reproducible-builds.org/pipermail/rb-general/2023-February/002856.html)], but Chris Lamb pointed out that this was due to the use of Python's [`datetime.fromtimestamp`](https://docs.python.org/3/library/datetime.html#datetime.datetime.fromtimestamp) over [`datetime.utcfromtimestamp`](https://docs.python.org/3/library/datetime.html#datetime.datetime.utcfromtimestamp) [[...](https://lists.reproducible-builds.org/pipermail/rb-general/2023-February/002859.html)].

<br>

James Addison also was having issues with a Debian package: in this case, the [`alembic`](https://tracker.debian.org/pkg/alembic) package. Chris Lamb was also able to identify the [Sphinx documentation generator](https://www.sphinx-doc.org/en/master/) as the cause of the problem, and [provided a potential patch that might fix it](https://lists.reproducible-builds.org/pipermail/rb-general/2023-February/002862.html). This was later filed upstream [[...](https://github.com/sphinx-doc/sphinx/issues/11198)].

<br>

Anthony Harrison wrote to our list twice, first by [introducing himself and their background](https://lists.reproducible-builds.org/pipermail/rb-general/2023-February/002871.html) and later to mention the increasing relevance of Software Bill of Materials (SBOMs):

> As I am sure everyone is aware, there is a growing interest in [SBOMs] as a
> way of improving software security and resilience. In the last two years, the
> US through the Exec Order, the EU through the proposed Cyber Resilience Act
> (CRA) and this month the UK has issued a consultation paper looking at
> software security and SBOMs appear very prominently in each
> publication.&nbsp;[[...](https://lists.reproducible-builds.org/pipermail/rb-general/2023-February/002872.html)]

<br>

[![]({{ "/images/reports/2023-02/almalinux.png#right" | relative_url }})](https://retout.co.uk/2023/02/04/almalinux-and-sboms/)

[Tim Retout](https://retout.co.uk/) wrote a blog post discussing [AlmaLinux](https://almalinux.org/) in the context of CentOS, RHEL and supply-chain security in general&nbsp;[[...](https://retout.co.uk/2023/02/04/almalinux-and-sboms/)]:

> Alma are generating and publishing Software Bill of Material (SBOM) files for
> every package; these are becoming a requirement for all software sold to the
> US federal government. What’s more, they are sending these SBOMs to a third
> party ([CodeNotary](https://codenotary.com/)) who store them in some sort of
> [Merkle tree](https://en.wikipedia.org/wiki/Merkle_tree) system to make it
> difficult for people to tamper with later. This should theoretically allow
> end users of the distribution to verify the supply chain of the packages they
> have installed?

---

## Debian

[![]({{ "/images/reports/2023-02/debian.png#right" | relative_url }})](https://debian.org/)

* Vagrant Cascadian noted that the Debian *bookworm* distribution has finally surpassed *bullseye* for reproducibility: 96.1% vs. 96.0%, despite having over 3500 more packages in the distribution.

* Roland Clobus posted his [latest update of the status of reproducible Debian ISO images](https://lists.reproducible-builds.org/pipermail/rb-general/2023-February/002877.html) noting that "all major desktops build reproducibly with *bullseye*, *bookworm* and *sid*," with the caveat that "when non-free firmware is activated, some non-reproducible files are generated".

* FC Stegerman submitted a new [Intent to Package (ITP)](https://wiki.debian.org/ITP) bug report [representing an intention to package `repro-apk`](https://bugs.debian.org/1030768), a set of [scripts to make Android `.apk` files reproducible](https://github.com/obfusk/reproducible-apk-tools).

* 23 reviews of Debian packages were added, 24 were updated and 20 were removed this month adding to [our knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). A new issue was added and identified by Chris Lamb [[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/c2b3882c)], and the `timestamps_embedded_in_manpages_by_node_marked_man` issue has been marked as resolved [[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/5cb5d781)].

---

## F-Droid & Android

[![]({{ "/images/reports/2023-02/fdroid.png#right" | relative_url }})](https://f-droid.org/)

* This month, F-Droid added 21 apps published with reproducible builds (out of 33 new apps in total), the [overview of F-Droid apps published with Reproducible Builds](https://gitlab.com/obfusk/fdroid-misc-scripts/-/blob/master/reproducible/overview.md) now includes graphs, and there are now also some [graphs of F-Droid apps verified by the Verification Server](https://gitlab.com/obfusk/fdroid-misc-scripts/-/blob/master/verification/graphs.md).

* FC Stegerman noticed that [signatures made by older versions of Android Gradle plugin cannot be copied](https://github.com/obfusk/apksigcopier/issues/88) because the signing method differs too much from that used by [*apksigner*](https://developer.android.com/studio/command-line/apksigner) (and [*signflinger*](https://android.googlesource.com/platform/tools/base/+/studio-master-dev/signflinger/)).

* FC Stegerman also created a helpful HOWTO page on the [F-Droid Wiki](https://gitlab.com/fdroid/wiki/-/wikis/pages) detailing how to [compare and subsequently make APKs reproducible](https://gitlab.com/fdroid/wiki/-/wikis/HOWTO:-diff-&-fix-APKs-for-Reproducible-Builds).

* A long-running thread on [*Hiding data/code in Android APK embedded signatures*](https://lists.reproducible-builds.org/pipermail/rb-general/2023-February/thread.html#2828) continued on [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/) this month; [*apksigcopier*](https://github.com/obfusk/apksigcopier) `v1.1.1` and [*reproducible-apk-tools*](https://github.com/obfusk/reproducible-apk-tools) `v0.2.2` + `v0.2.3` were also [announced](https://lists.reproducible-builds.org/pipermail/rb-general/2023-February/002853.html) on the same list.

* Lastly, FC Stegerman reported two issues on Google's own issue tracker: one related to a non-deterministic "Dependency Info Block"&nbsp;[[...](https://issuetracker.google.com/issues/268071369)] and another about a "virtual entry" added by the [*signflinger*](https://android.googlesource.com/platform/tools/base/+/studio-master-dev/signflinger/) tool causing unexpected differences between signed and unsigned APKs&nbsp;[[...](https://issuetracker.google.com/issues/268071371)].

---

## [diffoscope](https://diffoscope.org)

[![]({{ "/images/reports/2023-02/diffoscope.png#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is our in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it can provide human-readable diffs from many kinds of binary formats.

This month, Chris Lamb released versions [`235`](https://diffoscope.org/news/diffoscope-235-released/) and [`236`](https://diffoscope.org/news/diffoscope-236-released/); Mattia Rizzolo later released version [`237`](https://diffoscope.org/news/diffoscope-237-released/).

Contributions include:
* Chris Lamb:
  * Fix compatibility with PyPDF2 (re. issue [#331](https://salsa.debian.org/reproducible-builds/diffoscope/-/issues/331)) [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/82a767d2)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/ff6d9bbd)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/ccf3c2a4)].
  * Fix compatibility with [ImageMagick](https://imagemagick.org) version 7.1&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/60ea9cc6)].
  * Require at least version 23.1.0 to run the [Black](https://github.com/psf/black) source code tests&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c45de0a1)].
  * Update `debian/tests/control` after merging changes from others&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/72e5b2a1)].
  * Don't write test data during a test&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/25dcd1e6)].
  * Update copyright years&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/be3973b0)].
  * Merged a large number of changes from others.

* Akihiro Suda edited the `.gitlab-ci.yml` configuration file to ensure that versioned tags are pushed to the container registry&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/637c2985)].

* Daniel Kahn Gillmor provided a way to migrate from PyPDF2 to pypdf ([#1029741](https://bugs.debian.org/1029742)).

* Efraim Flashner updated the tool metadata for `isoinfo` on [GNU Guix](https://guix.gnu.org/)&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/7d6ce503)].

* FC Stegerman added support for Android `resources.arsc` files&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/7cf77ed1)], improved a number of file-matching regular expressions&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/8d7762f6)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c988c3ad)] and added support for Android `dexdump`&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/1bb9b812)]; they also [fixed](https://salsa.debian.org/reproducible-builds/diffoscope/-/commit/f48fbe61) a test failure ([#1031433](https://bugs.debian.org/1031433)) caused by Debian's `black` package having been updated to a newer version.

* Mattia Rizzolo:
  * updated the release documentation&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/b17b0595)],
  * fixed a number of [Flake8](https://flake8.pycqa.org/en/latest/) errors&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/8f710cd5)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/6eb8d06f)],
  * updated the autopkgtest configuration to only install `aapt` and `dexdump` on architectures where they are available&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/61f7c2b3)], making sure that the latest diffoscope release is in a good fit for the upcoming Debian bookworm freeze.

---

## [reprotest](https://salsa.debian.org/reproducible-builds/reprotest)

[Reprotest](https://salsa.debian.org/reproducible-builds/reprotest) version 0.7.23 was uploaded to both [PyPI](https://pypi.org/) and Debian unstable, including the following changes:

* Holger Levsen improved a lot of documentation&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/296800e)][[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/82d585b)][[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/b2a6f6f)], tidied the documentation as well&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/e8d9476)][[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/84496fa)], and experimented with a new `--random-locale` flag&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/f76f6e1)].

* Vagrant Cascadian adjusted *reprotest* to no longer randomise the build locale and use a UTF-8 supported locale instead [[...]](https://salsa.debian.org/reproducible-builds/reprotest/-/commit/610e6cae) (re.&nbsp;[#925879](https://bugs.debian.org/925879), [#1004950](https://bugs.debian.org/1004950)), and to also support passing `--vary=locales.locale=LOCALE` to specify the locale to vary&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/a92f741)].

Separate to this, Vagrant Cascadian started a thread on our [mailing list](https://lists.reproducible-builds.org/listinfo/rb-general) questioning the [future development and direction of *reprotest*](https://lists.reproducible-builds.org/pipermail/rb-general/2023-February/002876.html).

---

## Upstream patches

The Reproducible Builds project detects, dissects and attempts to fix as many currently-unreproducible packages as possible. We endeavour to send all of our patches upstream where appropriate. This month, we wrote a large number of such patches, including:

* Bernhard M. Wiedemann:

    * [`aiohttp`](https://github.com/aio-libs/aiohttp/pull/7191) (build fails in the future)
    * [`diff-pdf`](https://bugzilla.opensuse.org/show_bug.cgi?id=1180471)
    * [`dpdk`](https://build.opensuse.org/request/show/1067125)
    * [`ebumeter`](https://build.opensuse.org/request/show/1066747) (CPU-related issue)
    * [`firecracker`](https://github.com/firecracker-microvm/firecracker/issues/3439) (hashmap ordering issue)
    * [`jhead/gcc`](https://bugzilla.opensuse.org/show_bug.cgi?id=1208386) (used random temporary directory name)
    * [`libhugetlbfs`](https://build.opensuse.org/request/show/1068331) (drop unused unreproducible file)
    * [`prosody`](https://build.opensuse.org/request/show/1066043) (generates nondeterministic example SSL certificates)
    * [`python-sqlalchemy-migrate`](https://build.opensuse.org/request/show/1067129) (clean files leftover by [Sphinx](https://www.sphinx-doc.org/en/master/))
    * [`tigervnc`](https://bugzilla.opensuse.org/show_bug.cgi?id=1208478) (random RSA key)

* Chris Lamb:

    * [#1030708](https://bugs.debian.org/1030708) filed against [`gap-browse`](https://tracker.debian.org/pkg/gap-browse).
    * [#1030714](https://bugs.debian.org/1030714) filed against [`cwltool`](https://tracker.debian.org/pkg/cwltool).
    * [#1030715](https://bugs.debian.org/1030715) filed against [`adacgi`](https://tracker.debian.org/pkg/adacgi).
    * [#1030724](https://bugs.debian.org/1030724) filed against [`node-marked-man`](https://tracker.debian.org/pkg/node-marked-man) ([forwarded upstream](https://github.com/kapouer/marked-man/pull/32)).
    * [#1030727](https://bugs.debian.org/1030727) filed against [`multipath-tools`](https://tracker.debian.org/pkg/multipath-tools).
    * [#1031030](https://bugs.debian.org/1031030) filed against [`ruby-pgplot`](https://tracker.debian.org/pkg/ruby-pgplot).
    * [#1031412](https://bugs.debian.org/1031412) filed against [`pysdl2`](https://tracker.debian.org/pkg/pysdl2).
    * [#1031829](https://bugs.debian.org/1031829) filed against [`gawk`](https://tracker.debian.org/pkg/gawk).
    * [#1032057](https://bugs.debian.org/1032057) filed against [`pyproject-api`](https://tracker.debian.org/pkg/pyproject-api).

* Gioele Barabucci:

    * [#1032056](https://bugs.debian.org/1032056) filed against [`systemtap`](https://tracker.debian.org/pkg/systemtap).

* Larry Doolittle:

    * [#1031711](https://bugs.debian.org/1031711) filed against [`verilator`](https://tracker.debian.org/pkg/verilator).

* Vagrant Cascadian:

    * [#1030270](https://bugs.debian.org/1030270) filed against [`libreoffice`](https://tracker.debian.org/pkg/libreoffice).

---

## Testing framework

[![]({{ "/images/reports/2023-02/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project operates a comprehensive testing framework (available at [tests.reproducible-builds.org](https://tests.reproducible-builds.org)) in order to check packages and other artifacts for reproducibility. In February, the following changes were made by Holger Levsen:

* Add three new [OSUOSL](https://osuosl.org/) nodes&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/d188805b)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f9f9c65d)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f57dbeb1)] and decommission the `osuosl174` node&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f05f9ce7)].
* Change the order of listed Debian architectures to show the 64-bit ones first&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/0b65129f)].
* Reduce the frequency that the Debian package sets and `dd-list` HTML pages update&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/965b4358)].
* Sort "Tested suite" consistently (and Debian *unstable* first)&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6503fafd)].
* Update the Jenkins shell monitor script to only query disk statistics every 230min&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7eafae2d)] and improve the documentation&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/5ed88c03)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/8d882964)].

---

## Other development work

[*disorderfs*](https://salsa.debian.org/reproducible-builds/disorderfs) version `0.5.11-3` was uploaded by Holger Levsen, fixing a number of issues with the manual page&nbsp;[[...](https://salsa.debian.org/reproducible-builds/disorderfs/commit/2c3df22)][[...](https://salsa.debian.org/reproducible-builds/disorderfs/commit/e92c9c2)][[...](https://salsa.debian.org/reproducible-builds/disorderfs/commit/76c9e78)].

<br>

[![]({{ "/images/reports/2023-02/opensuse.png#right" | relative_url }})](https://www.opensuse.org/)

Bernhard M. Wiedemann published another [monthly report about reproducibility within openSUSE](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/thread/5TURLU4YB6PL2ES5GFWUJIQS22Z4YRKZ/).

---

If you are interested in contributing to the Reproducible Builds project, please visit the [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. You can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)

 * Mastodon: [@reproducible_builds@fosstodon.org](https://fosstodon.org/@reproducible_builds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
