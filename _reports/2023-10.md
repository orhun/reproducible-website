---
layout: report
year: "2023"
month: "10"
title: "Reproducible Builds in October 2023"
draft: true
---

* [FIXME](https://mcis.cs.queensu.ca/publications/2023/emse_rahul.pdf)

* [Reproducible Builds for CPython source tarballs](https://sethmlarson.dev/security-developer-in-residence-weekly-report-14)

* [FIXME](Codethink generously replaced our old Moonshot-Slides, which they generously hosted since 2016, with new kvm arm64 hardware, which
  Holger added to tests.r-b.o/debian.)

* Blog post about a talk by Ken Thompson and the original Trusting Trust attack:
	Russ Cox posted https://research.swtch.com/nih and disseminates the original Ken Thompson compiler backdoor in this, together with a link https://research.swtch.com/v6/ to follow along in a simulator.
	More of historical importance, but definitely interesting and relevant to this group, which is also mentioned in the posting.
	The talk by Thompson which sparked all this is linked to as well at https://www.youtube.com/watch?v=kaandEt_pKw&t=643s

 * [FIXME: Benjamin Drung uploaded distro-info](https://tracker.debian.org/distro-info) 1.6 supporting [SOURCE_DATE_EPOCH](https://bugs.debian.org/1034422).
 
 * Bernhard M. Wiedemann:
    * [`elasticsearch`](https://github.com/elastic/elasticsearch-py/issues/2320) (FTBFS)
    * [`kitty`](https://github.com/kovidgoyal/kitty/pull/6685) (merged, sort, tar mtime)
    * [`libpinyin`](https://github.com/libpinyin/libpinyin/issues/162) (ASLR)
    * [`maildir-utils`](https://github.com/djcb/mu/pull/2569) (date in copyright)
    * [`sbcl`](https://sourceforge.net/p/sbcl/mailman/sbcl-devel/thread/3ebdd95c-c498-462f-9cfe-7d05a1ee0044%40suse.de/) (report timestamp+other)
    * [`Hyprland`](https://github.com/hyprwm/Hyprland/pull/3550) (merged, filesys (find/meson))
    * [`edje_cc`](https://git.enlightenment.org/enlightenment/efl/issues/41) (race, nondeterministic order)
    * [`MooseX`](https://github.com/maros/MooseX-App/pull/71) (merged, kanku toolchain, date from perl-MooseX-App)
    * [`qpid`](https://github.com/apache/qpid-proton/pull/411) (merged, sort)
    * [`fftw3`](https://github.com/FFTW/fftw3/issues/337) (random order)
    * [`rakudo`](https://github.com/rakudo/rakudo/pull/5426) (merged, sort readdir)
    * [`rakudo/moarvm`](https://github.com/rakudo/rakudo/issues/5427) (unknown, toolchain)
    * [`gutenprint`](https://sourceforge.net/p/gimp-print/source/merge-requests/9/) (merged, date)
    * [`OpenRGB`](https://gitlab.com/CalcProgrammer1/OpenRGB/-/issues/3675) ([fixed corruption](https://gitlab.com/CalcProgrammer1/OpenRGB/-/merge_requests/2103) filesys+parallelism)
    * [`OpenRGB`](https://gitlab.com/CalcProgrammer1/OpenRGB/-/merge_requests/2101) (merged, FTBFS)
    * [`python-numpy`](https://bugzilla.opensuse.org/show_bug.cgi?id=1216458) (random file names)
    * [`fdo-client`](https://bugzilla.opensuse.org/show_bug.cgi?id=1216293) (private keys)
    * [`SLOF`](https://gitlab.com/qemu-project/SLOF/-/merge_requests/1) (date)
    * [`mame`](https://github.com/mamedev/mame/pull/11651) (order)
    * [`gsoap`](https://sourceforge.net/p/gsoap2/patches/185/) (date, toolchain)
    * [`python3-pyside2`](https://bugreports.qt.io/browse/PYSIDE-2508) (order)
    * [`mingw32-binutils`](https://build.opensuse.org/request/show/1116036) + [`mingw64-binutils`](https://build.opensuse.org/request/show/1116040) (date, toolchain)
    * [`erlang-retest`](https://build.opensuse.org/request/show/1116208) (embedded zip timestamp)
    * [`python-pandas`](https://build.opensuse.org/request/show/1117743) (FTBFS)
    * [`python-quantities`](https://build.opensuse.org/request/show/1117898) (date)
    * [`spack`](https://build.opensuse.org/request/show/1118130) (cpu count)
    * [`openblas`](https://build.opensuse.org/request/show/1118201) (cpu count)
    * [`xemacs-packages`](https://build.opensuse.org/request/show/1119260) (drop date)
    * [`occt`](https://build.opensuse.org/request/show/1119524) (sort (not upstream))
    * [`mame`](https://build.opensuse.org/request/show/1119553) (order)
    * [`qemu`](https://build.opensuse.org/request/show/1121011) (date+workaround sphinx toolchain issue)
    * [`hub/golang`](https://github.com/golang/go/issues/63851) (toolchain random build path)

* [openSUSE monthly](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/thread/4QTSQCYBMF6QZYWIB63T46ILLTVGVMMJ/)

* FIXME https://discourse.nixos.org/t/nixos-reproducible-builds-minimal-installation-iso-successfully-independently-rebuilt/34756 +  https://news.ycombinator.com/item?id=38057591
